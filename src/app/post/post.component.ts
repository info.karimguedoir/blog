import { Component, OnInit, Input } from '@angular/core';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  constructor() { }
  @Input() post :{
      title: String;
      content: String;
      loveIts: number;
      created_at:  Date;
  }

      color : String = 'white';


  onLike(){
    this.post.loveIts++;
    
  }

  onDislike(){
    this.post.loveIts--;
    
  }

  getColor(){
    if(this.post.loveIts>0){
      this.color = 'green';
    }
    else if(this.post.loveIts < 0){
      this.color = 'red';
    } else this.color = 'white';
    return this.color;
  }

  ngOnInit() {
    
  }

}
